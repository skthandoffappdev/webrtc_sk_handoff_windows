// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

module devtools_service;

// The DevTools service exposes two interfaces:
// - DevToolsCoordinator: Privileged interface used by the shell to setup the
//   service and perform other control operations. Other applications are not
//   able to request this interface.
// - DevToolsAgentClient: Interface that DevTools agents (e.g., Web page
//   renderers) use to report DevTools command results and events to the
//   DevTools service.
//
// DevTools agents need to implement the DevToolsAgent interface in order to
// receive DevTools commands from the DevTools service. Usually when an agent
// starts, it connects to the DevTools service, requests the DevToolsAgentClient
// interface, and then calls DevToolsAgentClient.RegisterAgent() to give the
// service a DevToolsAgent interface pointer.

interface DevToolsCoordinator {
  // Initializes the DevTools service. An HTTP server will be run on the
  // specified port and speak the Chrome remote debugging protocol.
  Initialize(uint16 remote_debugging_port);
};

interface DevToolsAgentClient {
  RegisterAgent(DevToolsAgent agent);

  // TODO(yzshen): Add methods to report DevTools command results and events.
};

interface DevToolsAgent {
  // TODO(yzshen): Add methods to accept DevTools commands.
};
